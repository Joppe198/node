## Node harjoituksissa käytetyt npm paketit.
- npm init Luodaan node projekti
- npm install -g nodemon Nodemon on työkalu, joka auttaa kehittämään Node.js-pohjaisia sovelluksia käynnistämällä Node.js-sovelluksen automaattisesti uudelleen,
- npm install experss Otetaan käyttöön express-moduuli
- npm install dotenv --save 
- npm install mongoose Otetaankäytöön mongodb-moduuli