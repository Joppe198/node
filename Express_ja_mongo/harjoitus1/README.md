## Harjoitus1
### Users REST API
Harjoituksessa tehdään pieni REST API:n käyttämällä Node.js:ää ja Expressiä. 
Harjoitus ei oikeasti tallenna JSON-dataan, vaan muutokset ovat näkyvillä yhden istunnon aikana.
## Get-Metodi, palauttaa kaikki käyttäjät JSON-datasta (read) 
Testaa GET-pyyntöä localhost:3000/users joka näyttää kaikki käyttäjät JSON-datasta, käyttämällä Postmania.
![Harjoitus1](img/get_metodi_kaikki_kayttajat.png "Harjoitus1")
## Get-metodi, palauttaa yhden käyttäjän JSON-datasta (read)
Testaa GET-pyyntöä localhost:3000/users joka näyttää yhden käyttäjän JSON-datasta, käyttämällä Postmania.
### Käyttäjä 1:
![Harjoitus1](img/get_metodi_kayttaja1.png "Harjoitus1")
### Käyttäjä 2:
![Harjoitus1](img/get_metodi_kayttaja2.png "Harjoitus1")
### Get-metodi, palauttaa yhden käyttäjän JSON-datasta (read)
Edelliseen harjoitukseen osaan päivitettiin koodia siten että jos haetaan sellaista käyttäjä id joka ei löydy JSON-datasta lähetetään 404 - not found ilmoitus.
![Harjoitus1](img/get_metodi_kayttajaa_ei_loydy.png "Harjoitus1")
## Delete-metodi, poistetaan käyttäjä JSON-datasta (delete)
Testataan Delete-pyyntöä localhost:3000/users/1 joka poistaa yhden käyttäjän JSON-datasta, käyttämällä Postmania.
![Harjoitus1](img/delete_kayttajan_poisto.png "Harjoitus1") <br>
Tarkistetaan Get-pyynnöllä localhost:3000/users että käyttäjä on poistettu JSON-datasta, käytämällä Postmania.
![Harjoitus1](img/get_kayttajan_poiston_jalkeen.png "Harjoitus1")
## PUT-pyyntö, päivittää käyttäjä tiedot (update)
Put-pyynnöllä voidaan päivittää käyttäjän tietoja JSON-datassa. <br>
Ekaksi haetaan GET-pyynöllä tiedot JSON-datasta. <br>
![Harjoitus1](img/get_metodi_kaikki_kayttajat.png "Harjoitus1") <br>
Seuraavaksi tehdään PUT-pyyntö jolla muokataan käyttäjä dataa palvelimella. <br>
Käyttämällä Postmanin Query Params-kohtaan jolla voidaan muokatta käyttäjän tietoja, harjoituksessa muutetaan Kirsi Kernel Testi Testaajaksi. <br>
![Harjoitus1](img/Put_pyyntö.png "Harjoitus1") <br>
Seuraavaksi GET-Pyynnöllä Tarkistetaan JSON-datasta että Käyttäjän tiedot ovat muuttuneet. <br>
![Harjoitus1](img/get_pyyntö.png "Harjoitus1") <br>
## POST-pyyntö, lisätään uusi käyttäjä (create)
POST-pyynnöllä voidaan lisätä uusia käyttäjiä JSON-dataan. <br>
![Harjoitus1](img/POST_pyynto.png "Harjoitus1") <br>
Jokaisella userilla on yksilöllinen id arvo JSON-datassa. <br>
Yleisesti tietokanta auttaa tässä, mutta nyt meillä ei ole vielä tietokantaa käytössä, joten meidän tulee generoida se jotenkin. <br> 
Tässä esimerkissä loopataan läpi kaikki jo JSON-datassa olevat userit ja etsitään sieltä isoin id:n arvo. <br>
Käytetään tätä tietoa apuna uuden käyttäjän id-arvon luonnissa (lisätään yhdellä). <br>
Nyt JavaScriptin map-funktio käy kaikki userit läpi ja palautta arvot Math.max()-funktiolle, joka palauttaa isoimmain käytössä olevan id:n arvon. <br>
Uusi useri liitetään users-kokoelmaan JavaScriptin concat-funktiolla. <br>
Lähetetään uusi POST-pyyntö jossa pitäisi nähdä uuden käyttäjän tiedot. <br>
![Harjoitus1](img/POST_pyynto2.png "Harjoitus1") <br>
Viimeiseksi lähetetään GET-pyyntö jolla varmistetaan että tieto on muuttunut JSON-datassa onnistuneesti. <br>
![Harjoitus1](img/GET_pyynto_paivitys.png "Harjoitus1") <br>