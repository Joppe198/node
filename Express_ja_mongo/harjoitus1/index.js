const { request, response } = require('express')
const express = require('express')
const app = express()
const port = 3000

app.use(express.json())
app.listen(port, () => {
    console.log('Harjoitus app kuuntelee porttia 3000')
})

let users = 
[
    {'id':'1', 'name':'Kirsi Kernel' },
    {'id':'2', 'name':'Matti Mainio'}
]

// Get pyyntö jolla haetaan kaikki käyttäjät
app.get('/users', (request, response) => {
    response.json(users)
})

// Get pyyntö jolla haetaan yhksi käyttäjän
app.get('/users/:id', (request, response) => {
    const {id} = request.params
    const user = users.find(user => user.id === id)
    // Tarkistus onko käyttäjä JSON-datassa vai lähetetäänkö 404
    if (user) response.json(user)
    else response.status(404).end()
})

// Delete pyyntö jolla poistetaan yksi käyttäjä
app.delete('/users/:id', (request, response) => {
    const {id} = request.params
    users = users.filter(user => user.id !== id)
    // lähetetään "204 no content" status koodin takaisin
    response.status(204).end()
})

// Put pyyntö jolla päivitetään käyttäjä tietoja
app.put('/users/:id', (request, response) => {
    const {id} = request.params
    const {name} = request.query
    const user = users.find(user => user.id === id)
    if (user) {
        user.name = name
        response.status(200).end()
    } else {
        response.status(204).end()
    }
})

// Post pyyntö luo uuden käyttäjän
app.post('/users', (request, response) => {
    const maxId = Math.max(...users.map(user => user.id), 0)
    const user = request.body
    user.id = (maxId+1).toString()
    users = users.concat(user)
    response.json(user)
})