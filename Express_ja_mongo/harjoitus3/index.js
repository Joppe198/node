const { request, response } = require('express')
const express = require('express')
const app = express()
const port = 3000
require('dotenv').config();

app.use(express.json())
app.listen(port, () => {
    console.log('Harjoitus app kuuntelee porttia 3000')
})
// otetaan käyttöön mongoose kirjasto
const mongoose = require('mongoose')

// yhdistetään tietokantaan
mongoose.connect(process.env.MONGODB_URI, {useNewUrlParser: true, useUnifiedTopology: true})
const db = mongoose.connection

// yhdistetään tietokantaan
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function() {
  console.log("tietokannan yhteyden testaaminen")
})

// new schema
const userSchema = new mongoose.Schema({
    name: String
})

// new model
const User = mongoose.model('User', userSchema, 'users')

const personSchema = new mongoose.Schema({
    name: { type: String, required: true },
    age: { type: Number, min: 0 },
    email: String
  })

// Get pyyntö jolla haetaan kaikki käyttäjät
app.get('/users', async (request, response) => {
    const users = await User.find({})
    response.json(users)
  })

// Get pyyntö jolla haetaan yhksi käyttäjän
app.get('/users/:id', async (request, response) => {
    const user = await User.findById(request.params.id)
    if (user) response.json(user)
    else response.status(404).end()
  })

// Delete pyyntö jolla poistetaan yksi käyttäjä
app.delete('/users/:id', async (request, response) => {
    const deletedUser = await User.findByIdAndRemove(request.params.id)
    if (deletedUser) response.json(deletedUser)
    else response.status(404).end()
  })

// Put pyyntö jolla päivitetään käyttäjä tietoja
app.put('/users/:id', (request, response) => {
    const {id} = request.params
    const {name} = request.query
    const user = users.find(user => user.id === id)
    if (user) {
        user.name = name
        response.status(200).end()
    } else {
        response.status(204).end()
    }
})

// Post pyyntö luo uuden käyttäjän
app.post('/users', async (request, response) => {
    // Get name from request
    const { name } = request.body

    // Create a new user
    const user = new User({
        name: name
    })

    // Save to db and send back to caller
    const savedUser = await user.save()
    response.json(savedUser)  
})