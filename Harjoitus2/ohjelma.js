// ohjelma.js
const user = require('./user')

let etunimi = "Joni"
let sukunimi = "Peltomaa"
let kaupunki = "Jyväskylä"
let paiva = "17.02.1988"

console.log(`${user.getName(etunimi , sukunimi)} lives in ${user.getLocation(kaupunki)} and was born on ${paiva}`)