## Tehtävän Kuvaus
Tehtävänä on tehdä User-moduuli seuraavilla ehdoilla:
- Moduulissa tulee olla getName toiminto, joka palauttaa nimesi.
- Moduulissa tulee olla getLocation toiminto, joka palauttaa nykyisen asuinkaupunkisi
- moduulissa tulee olla vakio, joka määrittelee syntymäpäiväsi dd.mm.yyyy
  
Määritte kaikki toiminnot ja vakio-arvo näkyville moduulin ulkopuolelle. Ota moduuli käyttöösi Node.js-sovellukseen ja käytä kaikkia toimintoja ja ominaisuuksia. Tulosta tiedot konsolille.

## Kuva
![Harjoitus2](harjoitus2.png "Harjoitus2")